# TAGMining #

Mining tags from text in WARC files.

### Project tagmining structure

	+ tagmining
	|---+ data
		|--+ file.warc.gz*
		+ lib
		|--+ jsoup library
		+ output
		|--+ out_1*
		|--+ out_2*
		|--+ out_3*
		|--+ extract
		|  |--+ (extracted files from warc)*
		|--+ src
		|  |----+ extractor
		|  |	|---+ ExtrWarc.java
		|  |----+ tagminer
		|  |	|---+ TagMiner.java
		|  |----+ tokenizer
		|  |	|---+ SentencesTokenizer.java
		|  |-----+ util
		|  		 |---+ WarcHTMLResponseRecord.java
		|		 |---+ WarcRecord.java
		|-+ pom.xml

	*: file not present in package or generated after tagminer execution.

## Project description:

In project, folder data contains the input warc file.
The lib folder contains jsoup jar, the library used for parsing html content.
In output folder there are: output files and a folder named extract, that contains 
extracted file from input warc file.
In source we have the packages of whole project.
In package extractor there's the ExtrWarc class. This class extracts the content of
warc file and store each html pages (in extract folder).
In package tagminer there's the TagMiner class. This class tags the sentences generated
from SentencesTokenizer class.
In package tokenizer there's the SentencesTokenizer class. This class takes the extracted 
file in input and tokenizes each sentence.