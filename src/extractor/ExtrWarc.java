package extractor;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import tagminer.TagMiner;
import util.*;

public class ExtrWarc {

	private final static String DATA_PATH= "./data"; // path to data.
	private final static String WORKSPACE_PATH= "./output"; // path to out.
	// instance variable:
	private Map<String,String> path2id; // Map with key: file path and value: TREC-ID.

	// Constructor:
	public ExtrWarc(){
		this.path2id = new HashMap<String,String>();
	}

	// method for extract data from warc:
	public void extractAllFromWarc() throws FileNotFoundException, IOException {

		Writer xml_writer = null;
		DataInputStream inStream = null;
		// create a TagMiner Object:
		TagMiner tag_miner = new TagMiner();

		try{	
			int count = 0;
			// data input
			String inputWarcFile = DATA_PATH+"/01.warc.gz";
			// open our gzip input stream
			GZIPInputStream gzInputStream = new GZIPInputStream(new FileInputStream(inputWarcFile));
			// cast to a data input stream
			inStream = new DataInputStream(gzInputStream);

			// iterate through our stream
			WarcRecord thisWarcRecord;

			while ((thisWarcRecord=WarcRecord.readNextWarcRecord(inStream))!=null) {

				// Object for write output:
				xml_writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(WORKSPACE_PATH+"/extract/"+count+ ".txt"), "utf-8"));

				// see if it's a response record
				if (thisWarcRecord.getHeaderRecordType().equals("response")) {

					// it is - create a WarcHTML record
					WarcHTMLResponseRecord htmlRecord = new WarcHTMLResponseRecord(thisWarcRecord);
					Vector<String> htmlCode = new Vector<String>();        
					htmlCode = htmlRecord.getHTMLCode();

					// store in map:
					this.path2id.put(WORKSPACE_PATH+"/extract/"+count+ ".txt",htmlRecord.getTargetTrecID());

					// try to insert url in txt and format it well:					
					// print our data
					for(String s : htmlCode){

						// call method for tag url:
						s = tag_miner.tagURL(s,htmlRecord.getTargetTrecID());
						// parse current string:
						Document d = Jsoup.parse(s);
						// writing body:
						if(d.body() != null)				
							xml_writer.write(d.body().text()+"\n"); // take only text.

					}
				}				

				// close.
				xml_writer.close();

				// LIMITER:
				System.out.println("written: "+ count+".txt"+ " ..."); // check for blocking.
				count++;
				// REMOVE THIS BLOCK.
				// MANUAL BREAK FOR LIMIT RESULT IN LOCAL.
				if(count == 3000){
					System.out.println();
					System.out.println("### LIMITED ###");
					System.out.println("stopped from user.");
					System.out.println("number of written files: "+count+".");
					System.out.println();
					break;
				}
			}

			System.out.println("DONE!"); // finish!
		}
		catch (IOException ex){
			System.out.println("-- Error: "+ex.getMessage());
		}

		finally {
			try {
				inStream.close();

			} catch (Exception ex) {  
				System.out.println("Error: "+ex.getMessage());
			}
		}
	}

	public Map<String,String> getPath2id() {
		return path2id;
	}


	public void setPath2id(Map<String,String> path2id) {
		this.path2id = path2id;
	}
}


