package tagminer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import extractor.ExtrWarc;
import tokenizer.SentencesTokenizer;

public class TagMiner {

	private final static String WORKSPACE_PATH= "./output"; // output path.

	private static final String NUMBER_PATTERN = "\\d+"; 
	private static final String DIG_TIME_PATTERN = "\\d+:\\d+(:\\d+)?\\s+[AaPp][Mm]?";
	private static final String ORD_PATTERN = "\\d+(st|nd|rd|th)";
	private static final String HTML_A_TAG_PATTERN = "(?i)<a([^>]+)>(.+?)</a>"; 
	private static final String HTML_IMG_PATTERN = "(<a[^>]*)(href=)([^>]*?)(><img[^>]*>(.*?)(</img>)?</a>)"; 
	private static final String MONEY_PATTERN = "(\\$|usd|USD|€|euro|EURO|¥|jpy|JPY|₤|gbp|GBP|chf|CHF|cad|CAD|aud|AUD|nzd|NZD?)\\s?(\\d+(\\,|.?)(\\s?\\d+)*)\\s?(mn|bn|billion|million)?";
	private static final String LINK_PATTERN ="((https?)://|www\\.)[^\\s/$.?#].[^\\s]*"; 
	private static final String VERSION_PATTERN ="v(\\.)?(\\d+)(\\s)?(\\.\\d+)*\\s"; 
	private static final String UNIT_PATTERN = "\\b(\\d+(\\s\\d+)*)\\s?(mw|kw|ton|μg|μl|μgml|kg|kilogram|pound|mb|gb|pb|mg|oz|lb|rpm|mhz|g)(s?)\\b";
	private static final String DISTANCE_PATTERN = "\\b(\\d+(\\s\\d+)*)\\s?(km|kilometer|miles|ft|yd|cm)(s?)\\b"; 

	private static final String PHONE_PATTERN = "\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"; 
	private static final String TEXT_DATE_FORMAT_PATTERN = "(?:\\s*(Sun(?:day)?|Mon(?:day)?|Tue(?:sday)?|Wed(?:nesday?)|Thu(?:esday?)|Fri(?:day?)|Sat(?:day?)),?\\s*)?(0?[1-9]|[1-2][0-9]|3[01])?\\s?(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\\s?(19[0-9]{2}|[2-9][0-9]{3}|[0-9]{2})";
	private static final String NUMBER_DATE_FORMAT_PATTERN = "((\\d\\d)\\/(\\d\\d)\\/(\\d{2}|\\d{4})\\s|(\\d\\d)\\-(\\d\\d)\\-(\\d{2}|\\d{4})\\s|(\\d\\d)\\.(\\d\\d)\\.(\\d{2}|\\d{4})\\s)";

	// Constructor:
	public TagMiner() throws IOException, FileNotFoundException{

		Writer out_1 = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(WORKSPACE_PATH+"/out_1.txt"), "utf-8"));
		Writer out_2 = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(WORKSPACE_PATH+"/out_2.txt"), "utf-8"));
		Writer out_3 = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(WORKSPACE_PATH+"/out_3.txt"), "utf-8"));

		// prepare output files:
		out_1.write("TREC-ID"+"\t"+"STR"+"\t"+"#TAG"+"\n");
		out_2.write("TREC-ID"+"\t"+"ORIGINAL_STR"+"\n");
		out_3.write("TREC-ID"+"\t"+"MODIFIED_STR"+"\n");

		out_1.close();
		out_2.close();
		out_3.close();

	}

	// method for write out_1: "TREC-ID, STRING REPLACED, TAG".
	public static void writeOut1(String trec_id, String replaced, String tag){

		try{

			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(WORKSPACE_PATH+"/out_1.txt", true)));
			out.println(trec_id+"\t"+replaced+"\t"+tag);
			out.close();

		}catch (IOException e) {
			System.out.println(e);
		}
	}

	// method for write out_1: "TREC-ID, ORIGINAL STRING".
	public static void writeOut2(String trec_id, String original_sentence){

		try{

			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(WORKSPACE_PATH+"/out_2.txt", true)));
			out.println(trec_id+"\t"+original_sentence);
			out.close();

		}catch (IOException e) {
			System.out.println(e);
		}
	}

	// method for write out_1: "TREC-ID, MODIFIED SENTENCE".
	public static void writeOut3(String trec_id, String modified_sentence){

		try{

			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(WORKSPACE_PATH+"/out_3.txt", true)));
			out.println(trec_id+"\t"+modified_sentence);
			out.close();

		}catch (IOException e) {
			System.out.println(e);
		}
	}

	// method for tag url by regex.
	public String tagURL(String line, String trec_id){

		String tag_url = line;
		// parse line with jsoup.
		Document d = Jsoup.parse(tag_url);

		// is an "a" html tag?
		if(!d.select("a").isEmpty()){
			// is an "img" tag?
			if(!d.select("img").isEmpty()){
				tag_url = line.replaceAll(HTML_IMG_PATTERN,"#IMG"); // replace with regex.
				writeOut1(trec_id,d.select("img").attr("src"),"IMG"); // write in out_1.
			}
			else{
				// is an "href" tag?
				if(!d.select("a").attr("href").isEmpty()){

					String tmp = d.select("a").attr("href"); // aux string.

					// is an email?
					if(tmp.contains("mailto:")){
						tag_url = tag_url.replaceAll(HTML_A_TAG_PATTERN, "#MAIL"); // replace with regex.
						writeOut1(trec_id,d.select("a").text(),"MAIL"); // write in out_1.
					}
					// otherwise is a link.
					else 
					{
						tag_url = tag_url.replaceAll(HTML_A_TAG_PATTERN, "#URL"); // replace with regex.
						writeOut1(trec_id,d.select("a").text(),"URL"); // write in out_1.
					}
				}
			}	

			writeOut2(trec_id,d.text()); // write in out_2
			d = Jsoup.parse(tag_url); // re-parse string.
			writeOut3(trec_id,d.text()); // write it in out_3.
		}
		return tag_url;
	}

	// main method:
	public static void main(String[] args) throws IOException{

		ExtrWarc ew = new ExtrWarc(); // create an ExtrWarc object.
		ew.extractAllFromWarc(); // extract data.

		// exec tokenizer:
		SentencesTokenizer st = SentencesTokenizer.runTokenizer(ew.getPath2id());
		Set<String> all_trec_id = st.getId2sentences().keySet(); // all TREC-ID.

		for(String id : all_trec_id){
			runTagger(id,st.getId2sentences().get(id)); // exec runTagger method for each TREC-ID.
		}
	}

	// method runTagger:
	private static void runTagger(String id, List<String> list) {

		for(String line : list){
			// call generalTagger for each line and regex:
			line = generalTagger(id,line,PHONE_PATTERN,"PHONE"); 
			line = generalTagger(id,line,LINK_PATTERN,"LINK_TEXT"); 
			line = generalTagger(id,line,MONEY_PATTERN,"MONEY"); 
			line = generalTagger(id,line,VERSION_PATTERN,"VERSION"); 
			line = generalTagger(id,line,DISTANCE_PATTERN,"DISTANCE"); 
			line = generalTagger(id,line,TEXT_DATE_FORMAT_PATTERN,"TEXT_DATE");
			line = generalTagger(id,line,NUMBER_DATE_FORMAT_PATTERN,"NUMBER_DATE");
			line = generalTagger(id,line,UNIT_PATTERN,"UNIT"); 
			line = generalTagger(id,line,DIG_TIME_PATTERN,"TIME");
			line = generalTagger(id,line,ORD_PATTERN,"ORD");
			line = generalTagger(id,line,NUMBER_PATTERN,"NUM");

		}
	}

	// method for tag sentences:
	private static String generalTagger(String id, String sentence, String regex, String tag){

		String tmp = sentence.replaceAll(regex,"#"+tag+" "); // replace regex.

		//  check if equals:
		if(!tmp.equals(sentence)){
			// are equals, then:
			Pattern pattern = Pattern.compile(regex); // pattern.
			Matcher matcher = pattern.matcher(sentence); // match sentence with pattern
			// found match?
			if(matcher.find()){
				String mod = matcher.group(); // return the input subsequence matched by matcher.
				// write outputs:
				writeOut1(id,mod,tag);
				writeOut2(id,sentence);
				writeOut3(id,tmp);
			}
		}
		return tmp;
	}
}
