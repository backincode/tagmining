package tokenizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;

public class SentencesTokenizer {

	private final static String WORKSPACE_PATH= "./output"; // output path.
	// instance variables:
	private List<String> sentences;
	private List<String> tokens;
	private Map<String,List<String>> id2sentences;

	// Constructor:
	public SentencesTokenizer(){

		this.sentences = new ArrayList<String>();
		this.tokens = new ArrayList<String>();
		this.id2sentences = new HashMap<String,List<String>>();
	}

	public Map<String, List<String>> getId2sentences() {
		return id2sentences;
	}

	public void setId2sentences(Map<String, List<String>> id2sentences) {
		this.id2sentences = id2sentences;
	}

	public List<String> getSentences(){
		return this.sentences;
	}

	public void setSentences(List<String> arg){
		this.sentences = arg;
	}

	public List<String> getTokens(){
		return this.tokens;
	}

	// method for add sentence.
	public void addSentence(String text){
		this.sentences.add(text);
	}

	// method for add token.
	public void addToken(String text){
		this.tokens.add(text);
	}

	// method for put sentences in map.
	public void putSentences(List<String> sentences, String trec_id){
		this.id2sentences.put(trec_id, sentences);
	}

	/* Return all the sentences in the input file at the given path.
	 * Uses PTB3 traditional tokens for, i.e., parenthesis.
	 */
	public void sentences(String path){

		String stamp;
		DocumentPreprocessor dp = new DocumentPreprocessor(path);

		for (List<HasWord> sentence : dp) {
			stamp ="";
			for(HasWord word : sentence)
				stamp +=word.word()+" ";

			addSentence(stamp);
		}
		cleanSentences();
	}

	/* Return all the tokens in the input file at the given path.
	 * For option in the constructor see PTBTokenizer documentation.
	 */
	public void tokenizer(String path) throws FileNotFoundException{

		PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new FileReader(path),
				new CoreLabelTokenFactory(), "ptb3Escaping=false");

		while (ptbt.hasNext()) {
			CoreLabel label = ptbt.next();
			addToken(label.word());
		}
	}

	/* Clean sentences list: delete short sentences (1-3 words).
	 */

	public void cleanSentences(){

		List<String> clean = new ArrayList<String>();

		for (String s : sentences){
			String[] tokens = s.split("\\s");

			if(tokens.length>3) // too short sentence?
				clean.add(s); 
		}
		setSentences(clean);
	}

	// method for execute tokenizer.
	public static SentencesTokenizer runTokenizer(Map<String,String> path2id) throws IOException{

		SentencesTokenizer st = new SentencesTokenizer();

		// directory extracted file from warc.
		File dir = new File(WORKSPACE_PATH+"/extract");

		File[] directoryListing = dir.listFiles(); // all file.

		// if not empty:
		if(directoryListing != null){
			// iterate all files:
			for (File child : directoryListing){
				String path = child.toString();
				st.sentences(path);

				if(path2id.get(path)!=null) 
					st.putSentences(st.getSentences(),path2id.get(path)); // put in map.

			}
		}
		return st;
	}

}